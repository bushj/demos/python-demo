# Simple Python app that runs once daily at 2am CST using scheduled pipelines

This is a simple Python application that prints "Hello World" along with the current date and time. It uses [scheduled pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) to run once every 4 hours. You can use GitLab's [pipeline view](https://docs.gitlab.com/ee/ci/pipelines/#view-pipelines) to see the past pipeline executions and their output.

## Files

`hello-world.py`
This is the Python script that prints "Hello World" along with the current date and time.

`.gitlab-ci.yml`
This is the GitLab pipeline that, when executed, runs the `hello-world.py` script.

## Scheduling a pipeline

Pipelines can be scheduled by navigating to [Build > Pipeline schedules](https://gitlab.com/bushj/demos/python-demo/-/pipeline_schedules). This project uses an existing scheduled pipeline called "Run Every 4 Hours", that kicks off the pipeline every 4 hours.

### Viewing pipeline results

The [pipeline view page](https://gitlab.com/bushj/demos/python-demo/-/pipelines) can be used to view past pipeline executions. It can be used to see which pipelines have succeeded or failed. You can also view pipeline output by drilling down into the pipeline's jobs ([an example](https://gitlab.com/bushj/demos/python-demo/-/jobs/4761956846)).
